import styled, { css } from 'styled-components'

export const Button = styled.button<{
    primary?: boolean;
    secondary?: boolean;
    color?: string;
}>`
  background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0 1em;
  padding: 0.25em 1em;
  margin: 1rem;

  ${(props) =>
    props.primary === true &&
    css`
      background: darkgreen;
      color: white;
    `};

    ${(props) =>
    props.secondary === true &&
    css`
      background: darkred;
      color: white;
    `};

    ${(props) =>
    props.color &&
    css`
      color: ${props.color};
    `};
`;
