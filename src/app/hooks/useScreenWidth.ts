import { useEffect, useState } from "react";

export const useScreenWidth = (): {
  prevSWidth: number;
  sWidth: number;
} => {
  const [pWidth, setPWidth] = useState<number>(0);
  const [cWidth, setCWidth] = useState<number>(0);

  useEffect(() => {
    window.addEventListener("resize", handleResize, false);
    setPWidth(window.innerWidth);
    setCWidth(window.innerWidth);
    return () => {
        window.removeEventListener("resize", handleResize);
    }
  }, []);

  const handleResize = () => {
    setCWidth(window.innerWidth);
  };

  return {
    prevSWidth: pWidth,
    sWidth: cWidth,
  };
};
