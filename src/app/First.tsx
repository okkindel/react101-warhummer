import React from "react";
import { useEffect } from "react";
import { Button } from "./styles/Button";
import { FirstEl } from "./styles/FirstEl.styles";

interface Props {
  idx: number;
  name: string;
  surname?: string;
  children: JSX.Element;
  header?: JSX.Element
  isActive?: boolean;
  onClick: Function;
}

export const First = (props: Props): JSX.Element => {  
  const {
    idx,
    name,
    surname = "nazwisko",
    header,
    isActive = false,
    onClick,
    children,
  } = props;
  

  const giveMeName = (name: string): JSX.Element => {
    return (<p>Jestem sobie {name}</p>)
  }
  
  useEffect(() => {
    console.log("wywolalem sie", name);
    return () => {
      console.log('zamknąłem się', name);
    }
  }, [name]);

  return (
    <FirstEl>
      {header}
      <p>
        {isActive === true ? (
          <span>
            Im {name} {surname}.
          </span>
        ) : (
          <span>Im {name} - (inactive)</span>
        )}
      </p>

      {giveMeName('jacek')}

      <Button secondary onClick={() => onClick(idx)}>
        make me active / inactive
      </Button>

      {children}
    </FirstEl>
  );
};
