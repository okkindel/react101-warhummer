import { Button } from "./styles/Button";
import { useState } from "react";
import { useEffect } from "react";
import { First } from "./First";
import { useScreenWidth } from "./hooks/useScreenWidth";

interface Names {
  name: string;
  surname: string;
  active: boolean;
}

function App(): JSX.Element {
  const [names, setNames] = useState<Names[]>([]);
  const [age, setAge] = useState<number>(0);
  const { sWidth } = useScreenWidth();

  useEffect(() => {
    const names = [
      { name: "maciek", surname: "hajduk", active: true },
      { name: "michał", surname: "tymcio", active: false },
      { name: "piotr", surname: "kowalik", active: true },
    ];
    const correctedArray = names.filter((el, idx) => idx <= age);
    setNames(correctedArray);
  }, [age]);

  const onClick = (index: number): void => {
    const isNameActive = names[index].active;
    names[index].active = !isNameActive;
    setNames([...names]);
  };

  return (
    <div>
      Hello world
      <br />
      {age}
      <br />
      {names.map((el, idx) => (
        <First
          header={<h1>Jestem tytuł</h1>}
          idx={idx}
          name={el.name}
          surname={el.surname}
          key={el.surname}
          isActive={el.active}
          onClick={onClick}
        >
          <div>Jestem sobie w tagach</div>
        </First>
      ))}
      <br />
      {sWidth > 800 && (
        <Button
          primary
          color="pink"
          onClick={() => {
            setAge((el) => el + 1);
          }}
        >
          Age + 1
        </Button>
      )}
    </div>
  );
}

export default App;
